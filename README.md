# Click counter task

This programming task was made by Joonas Nygård for Liana Technologies. 


## Introduction

App has view with number and button. 
When user who is not logged in clicks the button number is incremented by 1. 
When user logged-in presses the button the number is incremented by 10.


## Getting started

You need to have [Meteor.js](https://www.meteor.com/install) installed

Clone repository, install dependencies and run the app
```bash
git clone https://JNygard@bitbucket.org/JNygard/lianatask.git
cd lianatask
meteor npm install
meteor
```


## Usage

Use the  browser console to create account, login and logout:

```bash
Accounts.createUser({username: 'example_account', password: 'example_password'})
Meteor.loginWithPassword('example_account', 'example_password')
Meteor.logout()
```




