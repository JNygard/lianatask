import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Lianatest = new Mongo.Collection('lianatest');

if (Meteor.isServer) {
    Meteor.publish('lianatest', function tasksPublication() {

        if (!Lianatest.findOne('liana')) {
            Lianatest.insert({
                _id: 'liana',
                clickCount: 0
            });
        }
        return Lianatest.find({ _id: 'liana' });
    });
}

Meteor.methods({
    'lianatest.increaseClickCount'() {

        const increase = this.userId ? 10 : 1
        const clicks = Lianatest.findOne('liana').clickCount

        Lianatest.update('liana', { $set: { clickCount: clicks + increase } });
    },
});