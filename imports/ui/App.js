import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Lianatest } from '../api/lianatest.js';


class App extends Component {

    increaseClick() {
        Meteor.call('lianatest.increaseClickCount');
    }

    render() {
        return (
            <div className="container">
                <header>
                    <h1>Click Counter</h1>
                    <span>{Meteor.user() ? "Logged in: " + Meteor.user().username + "" : "Not logged in "}</span>
                </header>
                <section className="content">

                    <h1>{this.props.liana ? this.props.liana.clickCount : "-"}</h1>

                    <div className="button-container">

                        <a className="button" onClick={this.increaseClick.bind(this)}>Nappula</a>

                    </div>
                </section>
            </div>
        );
    }
}

export default withTracker(() => {
    Meteor.subscribe('lianatest');

    return {
        liana: Lianatest.findOne('liana')
    };
})(App);